import React, { PureComponent } from "react";
import styles from "./Button.module.scss";

class Button extends PureComponent {
  render() {
    const { backCol, text, onClick } = this.props;

    return (
      <>
        <button
          onClick={onClick}
          className={styles.btn}
          style={{ backgroundColor: backCol}}
        >
          {text}
        </button>
      </>
    );
  }
}

export default Button;
