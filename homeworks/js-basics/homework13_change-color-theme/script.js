const head = document.querySelector('head');
let myBtn = document.querySelector('.button-change');
let link = document.createElement('link');

myBtn.addEventListener('click', function() {
    if(localStorage.getItem('theme') === null) {
        let link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = './styles/red.css';
        head.append(link)
        localStorage.setItem('theme', 'aqua');
    } else {
        let myLink = document.querySelector('link[href="./styles/red.css"]');
        myLink.remove();
        localStorage.clear();
    }
})

if (localStorage.getItem('theme') === null) {
        localStorage.clear();
} else {
    let link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = './styles/red.css';
        head.append(link)
        localStorage.setItem('theme', 'aqua');
}
