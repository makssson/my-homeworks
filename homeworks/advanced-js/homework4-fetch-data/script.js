const fetchResult = fetch("https://ajax.test-danit.com/api/swapi/films");

fetchResult
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    console.log(data);

    data.forEach((element) => {
      document.body.insertAdjacentHTML(
        "beforeend",
        `<p>Episode ${element.episodeId}. ${element.name}. ${element.openingCrawl}</p>`
      );

      const heroes = element.characters;
      heroes.forEach((el) => {
        fetch(el)
          .then((response) => {
            return response.json();
          })
          .then((info) => {
            document.body.insertAdjacentHTML(
                "beforeend",
                `<p>${info.name}</p>`
              );
            console.log(info.name);
          });
      });
    });
  });
