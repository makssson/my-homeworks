import React, { Component } from "react";
import "./App.css";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import styles from "./components/Modal/Modal.module.scss";

class App extends Component {
  state = {
    isOpenFirst: false,
    isOpenSecond: false,
  };

  openModalFirst = (modalClosed = true) => {
    if (modalClosed) {
      this.setState({ isOpenFirst: true });
    }
  };

  openModalSecond = (modalClosed = true) => {
    if (modalClosed) {
      this.setState({ isOpenSecond: true });
    }
  };

  render() {
    const { isOpenFirst, isOpenSecond } = this.state;

    console.log(this.state);

    return (
      <div className="App">
        <Button
          text="Open first modal"
          backCol="#b3382c"
          onClick={() => this.openModalFirst()}
        />
        <Button
          text="Open second modal"
          backCol="green"
          onClick={() => this.openModalSecond()}
        />

        {isOpenFirst && (
          <Modal
            header="Do you want to delete this file ?"
            text="Once you delete this file, it won’t be possible to undo this
              action. Are you sure you want to delete it?"
            actions={
              <>
                <button className={styles.modalButtonOk}>Ok</button>
                <button className={styles.modalButtonCancel}>Cancel</button>
              </>
            }
            closeButton={true}
            close={(e) => {
              if (e.target === e.currentTarget) {
                this.setState({ isOpenFirst: false });
              }
            }}
          ></Modal>
        )}

        {isOpenSecond && (
          <Modal
            header="Second try to delete"
            text="Warning again, dont delete this file"
            actions={
              <>
                <button className={styles.modalButtonOk}>Yes</button>
                <button className={styles.modalButtonCancel}>No</button>
              </>
            }
            closeButton={true}
            close={(e) => {
              if (e.target === e.currentTarget) {
                this.setState({ isOpenSecond: false });
              }
            }}
          ></Modal>
        )}
      </div>
    );
  }
}

export default App;
