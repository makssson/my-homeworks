/* eslint-disable jsx-a11y/alt-text */
import { React, useState } from "react";
import Button from "../Button/Button";
import styles from "./Card.module.scss";
import { useDispatch } from "react-redux";
import { ReactComponent as StarIcon } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";
import PropTypes from "prop-types";
import {
  openModalAC,
  openSecondModalAC,
} from "../../store/actionCreators/modalActionCreators";

const Card = (props) => {
  const { name, price, picture, display } = props;

  const [isFavourite, setIsFavourite] = useState(false);

  const dispatch = useDispatch();

  const openModal = () => {
    dispatch(openModalAC());
  };

  const openModalSecond = () => {
    dispatch(openSecondModalAC());
  };

  const modalHandler = () => {
    openModal();
    localStorage.setItem("card modal", name);
  };

  const secondModalHandler = () => {
    openModalSecond();
    localStorage.setItem("card modal", name);
  };

  const changeFavourite = () => {
    if (localStorage.getItem(`${name} Favourite`, "favourite") || isFavourite) {
      localStorage.removeItem(`${name} Favourite`, "favourite");
      setIsFavourite(false);
    } else {
      localStorage.setItem(`${name} Favourite`, "favourite");
      setIsFavourite(true);
    }
  };

  return (
    <>
      <div className={styles.cardContainer} style={{ display: display }}>
        <div>
          <span className={styles.cardName}>{name}</span>
          <span className={styles.cross} onClick={secondModalHandler}>
            X
          </span>
        </div>
        {localStorage.getItem(`${name} Favourite`, "favourite") ? (
          <StarRemove onClick={changeFavourite} />
        ) : (
          <StarIcon onClick={changeFavourite} />
        )}
        <p>Price: {price}</p>
        <img className={styles.cardImg} src={picture} />
        <Button
          id={name}
          text="Add to cart"
          backCol="#b3382c"
          onClick={modalHandler}
        />
      </div>
    </>
  );
};

Card.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
};

export default Card;
