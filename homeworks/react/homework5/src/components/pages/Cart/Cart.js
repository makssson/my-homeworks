import { React, useState, useEffect } from "react";
import Card from "../../Card/Card";
import { useDispatch, useSelector } from "react-redux";
import Modal from "../../Modal/Modal";
import styleses from "../../Modal/Modal.module.scss";
import { closeSecondModalAC } from "../../../store/actionCreators/modalActionCreators";
import MainForm from "../../MainForm/MainForm";


const Cart = (props) => {
  const [items, setItems] = useState([]);

  const dispatch = useDispatch();
  const isOpenSecond = useSelector(({modal}) => modal.isOpenSecond);
  const isCheckout = useSelector(({checkout}) => checkout.isCheckout)
  

  const closeModalSecond = () => {
    dispatch(closeSecondModalAC());
    
  };

  useEffect(() => {
    async function fetchGoods() {
      const result = await fetch("./goods.json").then((res) => res.json());

      setItems(result);
    }
    fetchGoods();
  }, []);

  return (
    <>
      <section>
        <MainForm/>
        <h1>Cart:</h1>
        {!isCheckout && items.map(({ vc, name, ...args }) =>
          localStorage.getItem(name, "in basket") ? (
            <Card key={vc} name={name} {...args} />
          ) : (
            <Card key={vc} name={name} {...args} display="none" />
          )
        )}
      </section>

      {isOpenSecond && (
        <Modal
          header="Do you want to remove from cart ?"
          text="Once you delete this good, it will be possible to add this
              good again. Are you sure you want to delete it?"
          actions={
            <>
              <button
                className={styleses.modalButtonOk}
                onClick={() => {
                  const pickedGood = localStorage.getItem("card modal");
                  localStorage.removeItem(pickedGood, "in basket");
                }}
              >
                Ok
              </button>
              <button className={styleses.modalButtonCancel}>Cancel</button>
            </>
          }
          closeButton={true}
          close={(e) => {
            if (e.target === e.currentTarget || e.target.innerText === "Ok") {
              closeModalSecond();
            }
          }}
        ></Modal>
      )}
    </>
  );
};

export default Cart;
