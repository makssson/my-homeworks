import React from "react";
import styles from "./Header.module.scss";
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <header className={styles.header}>
      <nav>
        <ul className={styles.navList}>
          <li>
            <NavLink exact to="/"  activeClassName={styles.active}>
              Home
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/favourite" activeClassName={styles.active}>
              Favourite
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/cart" activeClassName={styles.active}>
              Cart
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
