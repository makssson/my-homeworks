import { React } from "react";
import Card from "../Card/Card";
import styles from "./Container.module.scss";
import Modal from "../Modal/Modal";
import styleses from "../Modal/Modal.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { closeModalAC } from "../../store/actionCreators/modalActionCreators";
import { getGoodsAC } from "../../store/actionCreators/modalActionCreators";

const Container = () => {

  const dispatch = useDispatch();
  const isOpenFirst = useSelector(({modal}) => modal.isOpenFirst);
  const goods = useSelector(({modal}) => modal.goods);
  
  const closeModal = () => {
    dispatch(closeModalAC());
  };

  const goodsHandle = () => {
    dispatch(getGoodsAC());
  };

  return (
    <section>
      <h1>Goods</h1>
      <button onClick={goodsHandle}>GET GOODS</button>
      <div className={styles.goodsContainer}>
        {goods.map(({ vc, ...args }) => (
          <Card key={vc} {...args} />
        ))}
      </div>
      {isOpenFirst && (
        <Modal
          header="Do you want to add to cart ?"
          text="Once you add this good, it won’t be possible to undo this
                action. Are you sure you want to add it?"
          actions={
            <>
              <button
                className={styleses.modalButtonOk}
                onClick={() => {
                  const pickedGood = localStorage.getItem("card modal");
                  localStorage.setItem(pickedGood, "in basket");
                }}
              >
                Ok
              </button>
              <button className={styleses.modalButtonCancel}>Cancel</button>
            </>
          }
          closeButton={true}
          close={(e) => {
            if (e.target === e.currentTarget || e.target.innerText === "Ok") {
              closeModal();
            }
          }}
        ></Modal>
      )}
    </section>
  );
};

export default Container;
