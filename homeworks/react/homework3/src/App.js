import React from "react";
import "./App.css";
import Header from "./components/Header/Header";
import { BrowserRouter as Router } from "react-router-dom/cjs/react-router-dom.min";
import Routes from "./Routes/Routes";


const App = () => {
 

  return (
    <Router>
      <div className="App">
        <Header />
        <Routes/>
      </div>
    </Router>
  );
};

export default App;
