import { React, useEffect, useState } from "react";
import Card from "../Card/Card";
import styles from "./Container.module.scss";

const Container = () => {
  const [allGoods, setAllGoods] = useState([]);

  useEffect(() => {
    async function fetchGoods() {
      const goods = await fetch("./goods.json").then((res) => res.json());
      setAllGoods(goods);
    }
    fetchGoods();
  }, []);

  return (
    <section>
      <h1>Goods</h1>
      <div className={styles.goodsContainer}>
        {allGoods.map(({ vc, ...args }) => (
          <Card key={vc} {...args} />
        ))}
      </div>
    </section>
  );
};

export default Container;
