import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import styles from "./MainForm.module.scss";
import { useDispatch } from "react-redux";
import { checkoutAC } from "../../store/actionCreators/checkoutActionCreators";

const initialValues = {
  name: "",
  surname: "",
  age: "",
  adress: "",
  mobile: "",
};

const validationSchema = yup.object().shape({
  name: yup
    .string()
    .required("Field required")
    .min(2, "At least 2 letters required"),
  surname: yup
    .string()
    .required("Field required")
    .min(2, "At least 2 letters required"),
  age: yup
    .number()
    .required("Field required")
    .min(18, "You must be at least 18 years old"),
  adress: yup
    .string()
    .required("Field required")
    .min(2, "At least 2 letters required"),
  mobile: yup
    .string()
    .required("Field required")
    .length(12, "Number must have 12 characters"),
});

const MainForm = () => {
  const dispatch = useDispatch();

  const checkoutBasket = () => {
    dispatch(checkoutAC());
  };

  const onSubmit = (values) => {
    console.log(values);
    for(let i=0; i<localStorage.length; i++) {
      let item = localStorage.key(i)
      console.log(item);
    }
    checkoutBasket()
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      {() => {
        return (
          <Form>
            <h2>Form</h2>
            <div className={styles.mainForm}>
              <Field type="text" name="name" placeholder="Name" />
              <ErrorMessage
                name="name"
                render={(msg) => <p className={styles.errorMessage}>{msg}</p>}
              />

              <Field type="text" name="surname" placeholder="Surname" />
              <ErrorMessage
                name="surname"
                render={(msg) => <p className={styles.errorMessage}>{msg}</p>}
              />

              <Field type="number" name="age" placeholder="Age" />
              <ErrorMessage
                name="age"
                render={(msg) => <p className={styles.errorMessage}>{msg}</p>}
              />

              <Field type="text" name="adress" placeholder="Adress" />
              <ErrorMessage
                name="adress"
                render={(msg) => <p className={styles.errorMessage}>{msg}</p>}
              />

              <Field type="text" name="mobile" placeholder="Mobile" />
              <ErrorMessage
                name="mobile"
                render={(msg) => <p className={styles.errorMessage}>{msg}</p>}
              />
            </div>
            <button type="submit">Checkout</button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default MainForm;
