import React from "react";
import { Switch, Route } from "react-router-dom/cjs/react-router-dom.min";
import Favourite from "../components/pages/Favourite/Favourite";
import Cart from "../components/pages/Cart/Cart";
import HomePage from "../components/pages/HomePage/HomePage";


const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <HomePage/>
      </Route>
      <Route exact path="/favourite">
        <Favourite />
      </Route>
      <Route exact path="/cart">
        <Cart />
      </Route>
    </Switch>
  );
};

export default Routes;
