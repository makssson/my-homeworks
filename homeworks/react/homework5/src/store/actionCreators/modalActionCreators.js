import {
  CLOSE_MODAL,
  CLOSE_MODAL_SECOND,
  GET_GOODS,
  OPEN_MODAL,
  OPEN_MODAL_SECOND,
} from "../actions/modalActions";

export const openModalAC = () => ({ type: OPEN_MODAL });
export const closeModalAC = () => ({ type: CLOSE_MODAL });
export const openSecondModalAC = () => ({ type: OPEN_MODAL_SECOND });
export const closeSecondModalAC = () => ({ type: CLOSE_MODAL_SECOND });

export const getGoodsAC = () => async (dispatch) => {
  const data = await fetch("./goods.json").then((res) => res.json());

  dispatch({ type: GET_GOODS, payload: data });
};
