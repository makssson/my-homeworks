const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];
const root = document.getElementById("root");

class List {
  constructor(className) {
    this.className = className;
  }

  render() {
    root.insertAdjacentHTML(
      "beforeend",
      `
      <ul class="${this.className}">
      </ul>
  
        `
    );
  }
}

const testList = new List("book__list");
testList.render();

function listRender() {
  const list = document.querySelector(".book__list");
  books.forEach((el, i) => {
    try {
      let objNum = i + 1;
      if (el.author === undefined) {
        throw new Error(`Нет имени автора в обьекте ${objNum}`);
      } else if (el.name === undefined) {
        throw new Error(`Нет названия в обьекте ${objNum}`);
      } else if (el.price === undefined) {
        throw new Error(`Нет цены в обьекте ${objNum}`);
      } else {
        list.insertAdjacentHTML(
          "beforeend",
          `<li>${el.author}, ${el.name}, ${el.price}</li>`
        );
      }
    } catch (err) {
      console.error(err);
    }
  });
}
listRender();

// class BookItem extends List {
//   constructor(className, author, name, price) {
//     super(className);
//     this.author = author;
//     this.name = name;
//     this.price = price;
//   }

//   render() {
//     const list = document.querySelector(".book__list");
//     list.insertAdjacentHTML(
//       "beforeend",
//       `<li class="${this.className}">${this.author} ${this.name} ${this.price}</li>`
//     );
//   }
// }
// const testItem = new BookItem("book__item", "z", "ffwf", 123);
// testItem.render();
