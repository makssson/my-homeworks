let items = ['hello', 'world', 23, '23', null]

function filterBy(array, type) {

    let filteredArr = array.filter(function(element) {
        return typeof element !== type;
    })

    return filteredArr;
}


console.log(filterBy(items, "string"));

