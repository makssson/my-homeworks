import Modal from "./Modal";
import {render} from "@testing-library/react"


describe('Modal render', () => {
    test('Should Modal match snapshot', () =>{
        // eslint-disable-next-line testing-library/render-result-naming-convention
        const { asFragment } = render(<Modal header="Some header" text="Some text" close={() => {}}/>)

        expect(asFragment()).toMatchSnapshot();
    })
})