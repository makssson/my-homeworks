class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  set nameInfo(newName) {
    this.name = newName;
  }

  get nameInfo() {
    return this.name;
  }

  set ageInfo(newAge) {
    this.age = newAge;
  }

  get ageInfo() {
    return this.age;
  }

  set salaryInfo(newSalary) {
    this.salary = newSalary;
  }

  get salaryInfo() {
    return this.salary;
  }
}

const employee = new Employee("Maks", 24, 12000);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salaryInfo() {
    return this.salary * 3;
  }
}

const programmer = new Programmer("Vasya", 23, 10000, "JavaScript");

const programmer2 = new Programmer("Sasha", 22, 8000, "C++");

console.log(employee);
console.log(programmer);
console.log(programmer2);
