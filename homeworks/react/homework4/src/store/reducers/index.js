import { OPEN_MODAL, CLOSE_MODAL, OPEN_MODAL_SECOND, CLOSE_MODAL_SECOND, GET_GOODS } from "../actions/modalActions";


const initialState = {
  isOpenFirst: false,
  isOpenSecond: false,
  goods: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MODAL: {
      return { ...state, isOpenFirst: true };
    }
    case CLOSE_MODAL: {
      return { ...state, isOpenFirst: false };
    }
    case OPEN_MODAL_SECOND: {
      return { ...state, isOpenSecond: true };
    }
    case CLOSE_MODAL_SECOND: {
      return { ...state, isOpenSecond: false };
    }
    case GET_GOODS: {
      return { ...state, goods: action.payload };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
