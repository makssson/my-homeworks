const BASE_URL = "https://ajax.test-danit.com/api/json";

class Card {
  findUser() {
    return fetch(`${BASE_URL}/users`, { method: "GET" })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(response.status);
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  findPost() {
    return fetch(`${BASE_URL}/posts`, { method: "GET" })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(response.status);
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  deletePost(postId) {
    return fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: "DELETE",
    }).then((response) => {
      console.log(response);
    });
  }
  async renderCard() {
    const posts = await this.findPost();
    const users = await this.findUser();

    posts.forEach((el) => {
      users.forEach((elem) => {
        if (el.userId === elem.id) {
          document.body.insertAdjacentHTML(
            "beforeend",
            `
          <div id=${el.id} class=wrapper>
            <p>${el.id}.</p>
            <p class=name>Name: ${elem.name}</p>
            <p class=email>Email: ${elem.email}</p>
            <p class=title>${el.title}</p>
            <p class=body>${el.body}</p>
            <button class=button data-id=${el.id}>delete</button>
          </div>
          `
          );
        }
      });
    });
    const btn = document.querySelectorAll(".button");
    const div = document.querySelectorAll(".wrapper");
    btn.forEach((el) => {
      el.addEventListener("click", (event) => {
        let postId = event.target.dataset.id;
        this.deletePost(postId);
        div.forEach((e) => {
          if (postId === e.getAttribute("id")) {
            e.style.display = "none";
          }
        });
      });
    });
  }
}

const card = new Card();
card.renderCard();
