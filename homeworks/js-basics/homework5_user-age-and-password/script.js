function createNewUser(firstName, lastName, birthDay) {  

    let newUser = {
        firstName,
        lastName,
        birthDay,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge() {
            const today = Date.parse(new Date());
            const userBirth = Date.parse(`${this.birthDay.slice(6)}-${this.birthDay.slice(3, 5)}-${this.birthDay.slice(0, 2)}`);
            const age = ((today - userBirth) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
            if (age < today) {
                return `${age - 1}`;
            } else {
                return `${age}`;
            }
        },
        getPassword() {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthDay.slice(6)}`;
        }
    }
    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    
}

createNewUser(prompt("What is your name?"), prompt("What is your last name?"), prompt("What is your birthday?", "dd.mm.yyyy"));


