/* eslint-disable jsx-a11y/alt-text */
import React, { PureComponent } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import styles from "./Card.module.scss";
import styleses from "../Modal/Modal.module.scss";
import { ReactComponent as StarIcon } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";
import PropTypes from "prop-types";

class Card extends PureComponent {
  state = {
    isOpenFirst: false,
  };

  openModalFirst = (modalClosed = true) => {
    if (modalClosed) {
      this.setState({ isOpenFirst: true });
    }
  };

  render() {
    const { isOpenFirst, isFavourite } = this.state;
    const { name, price, picture } = this.props;

    const changeFavourite = () => {
      if (localStorage.getItem(`${name} Favourite`, "in favourite")) {
        this.setState({ isFavourite: false });
        localStorage.removeItem(`${name} Favourite`, "in favourite");
      } else {
        this.setState({ isFavourite: true });
        localStorage.setItem(`${name} Favourite`, "in favourite");
      }
    };

    return (
      <>
        <div className={styles.cardContainer}>
          <span className={styles.cardName}>{name}</span>
          {isFavourite ? (
            <StarRemove onClick={changeFavourite} />
          ) : (
            <StarIcon onClick={changeFavourite} />
          )}
          <p>Price: {price}</p>
          <img className={styles.cardImg} src={picture} />
          <Button
            text="Add to cart"
            backCol="#b3382c"
            onClick={() => this.openModalFirst()}
          />
        </div>

        {isOpenFirst && (
          <Modal
            header="Do you want to add to cart ?"
            text="Once you add this good, it won’t be possible to undo this
    action. Are you sure you want to add it?"
            actions={
              <>
                <button
                  className={styleses.modalButtonOk}
                  onClick={() => localStorage.setItem(name, "in basket")}
                >
                  Ok
                </button>
                <button className={styleses.modalButtonCancel}>Cancel</button>
              </>
            }
            closeButton={true}
            close={(e) => {
              if (e.target === e.currentTarget || e.target.innerText === "Ok") {
                this.setState({ isOpenFirst: false });
              }
            }}
          ></Modal>
        )}
      </>
    );
  }
}

Card.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
};

export default Card;
