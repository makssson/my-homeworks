import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";


const Button = (props) => {
  const { backCol, text, onClick, id } = props;

  

  return (
    <>
      <button
        onClick={onClick}
        className={styles.btn}
        style={{ backgroundColor: backCol }}
        id={id}
      >
        {text}
      </button>
    </>
  );
};

Button.propTypes = {
  backCol: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;
