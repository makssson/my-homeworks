const BASE_URL = "https://api.ipify.org/?format=json";

const BUTTON = document.querySelector(".button");

BUTTON.addEventListener("click", () => {
  const sendRequest = async function () {
    try {
      const response = await fetch(`${BASE_URL}`);
      if (response.status >= 200 && response.status < 300) {
        const data = await response.json();
        return data;
      }
      throw new Error(response.status);
    } catch (error) {
      throw new Error(error);
    }
  };

  const fetchIP = async function () {
    const IP = await sendRequest();
    try {
      const newData = await fetch(`http://ip-api.com/json/${IP.ip}`);
      if (newData.status >= 200 && newData.status < 300) {
        const data = await newData.json();
        return data;
      }
      throw new Error(newData.status);
    } catch (err) {
      throw new Error(err);
    }
  };

  const renderInfo = async function() {
      const info = await fetchIP();
      document.body.insertAdjacentHTML('beforeend', `
      <p>${info.country}, ${info.region}, ${info.city}, ${info.regionName}, ${info.org}</p>
      `)
  }

  renderInfo()
});
