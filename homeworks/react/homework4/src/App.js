import React from "react";
import "./App.css";
import Header from "./components/Header/Header";
import { BrowserRouter as Router } from "react-router-dom/cjs/react-router-dom.min";
import Routes from "./Routes/Routes";
import { Provider } from "react-redux";
import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Header />
          <Routes />
        </div>
      </Router>
    </Provider>
  );
};

export default App;
