let myUl = document.querySelector('.tabs');
let myTabs = document.querySelector('.tabs-content')


myUl.addEventListener('click', function(event) {
    myUl.querySelectorAll('.active').forEach(li => li.classList.remove('active'));

    event.target.classList.add('active');

    let targetTab = myUl.querySelector('.active');
    let dTitle = targetTab.getAttribute('data-title'); 

    let allTextData = myTabs.querySelectorAll('.tabs-text');
    allTextData.forEach(function (li) {
        if (dTitle === li.getAttribute('data-text')) {
            li.style.display = 'inline';
        } else {
            li.style.display = 'none';
        }
    })
})

