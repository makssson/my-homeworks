/* eslint-disable jsx-a11y/alt-text */
import React, { useState } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import styles from "./Card.module.scss";
import styleses from "../Modal/Modal.module.scss";
import { ReactComponent as StarIcon } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";
import PropTypes from "prop-types";

const Card = (props) => {
  const { name, price, picture, display } = props;

  const [isOpen, setIsOpen] = useState(false);
  const [isOpenSecond, setIsOpenSecond] = useState(false)
  const [isFavourite, setIsFavourite] = useState(false);

  const openModalFirst = (modalClosed = true) => {
    if (modalClosed) {
      setIsOpen(true);
    }
  };

  const openModalSecond = (modalClosed = true) => {
    if (modalClosed) {
      setIsOpenSecond(true)
    }
  }

  const changeFavourite = () => {
    if (localStorage.getItem(`${name} Favourite`, "favourite")) {
      setIsFavourite(false);
      localStorage.removeItem(`${name} Favourite`, "favourite");
    } else {
      setIsFavourite(true);
      localStorage.setItem(`${name} Favourite`, "favourite");
    }
  };

  return (
    <>
      <div className={styles.cardContainer} style={{ display: display }}>
        <div>
          <span className={styles.cardName}>{name}</span>
          <span className={styles.cross} onClick={() => openModalSecond()}>X</span>
        </div>
        {localStorage.getItem(`${name} Favourite`, "favourite") ? (
          <StarRemove onClick={changeFavourite} />
        ) : (
          <StarIcon onClick={changeFavourite} />
        )}
        <p>Price: {price}</p>
        <img className={styles.cardImg} src={picture} />
        <Button text="Add to cart" backCol="#b3382c" onClick={openModalFirst} />
      </div>

      {isOpen && (
        <Modal
          header="Do you want to add to cart ?"
          text="Once you add this good, it won’t be possible to undo this
                action. Are you sure you want to add it?"
          actions={
            <>
              <button
                className={styleses.modalButtonOk}
                onClick={() => localStorage.setItem(name, "in basket")}
              >
                Ok
              </button>
              <button className={styleses.modalButtonCancel}>Cancel</button>
            </>
          }
          closeButton={true}
          close={(e) => {
            if (e.target === e.currentTarget || e.target.innerText === "Ok") {
              setIsOpen(false);
            }
          }}
        ></Modal>
      )}
      {isOpenSecond && (
        <Modal
          header="Do you want to remove from cart ?"
          text="Once you delete this good, it will be possible to add this
                good again. Are you sure you want to delete it?"
          actions={
            <>
              <button
                className={styleses.modalButtonOk}
                onClick={() => localStorage.removeItem(name, "in basket")}
              >
                Ok
              </button>
              <button className={styleses.modalButtonCancel}>Cancel</button>
            </>
          }
          closeButton={true}
          close={(e) => {
            if (e.target === e.currentTarget || e.target.innerText === "Ok") {
              setIsOpenSecond(false);
            }
          }}
        ></Modal>
      )}
    </>
  );
};

Card.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
};

export default Card;
