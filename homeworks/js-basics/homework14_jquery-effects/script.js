$(document).ready(function(event) {
    let newUl = document.createElement('ul')
    $(newUl).addClass('new-menu')
    $(newUl).insertAfter('.hero-menu');
    $(newUl).css('display', 'flex')     

    for(let i = 4; i > 0; i--) {
        $('<a></a>').appendTo(newUl).addClass('new-menu-item').css('color', 'yellow').css('padding', '10px');
    }
    $('.new-menu .new-menu-item:nth-child(1)').text('Home').attr('href', '#home').wrap('<li/>');
    $('.new-menu .new-menu-item:nth-child(2)').text('Most popular posts').attr('href', '#posts').wrap('<li/>');
    $('.new-menu .new-menu-item:nth-child(3)').text('Our Clients').attr('href', '#clients').wrap('<li/>');
    $('.new-menu .new-menu-item:nth-child(4)').text('Hot news').attr('href', '#news').wrap('<li/>');

    $('.new-menu').wrap('<a id="home"></a>')
    $('.popular-posts').wrap('<a id="posts"></a>')
    $('.clients').wrap('<a id="clients"></a>')
    $('.hot-news').wrap('<a id="news"></a>')

    $('.new-menu').on('click','a', function (ev) {
        ev.preventDefault();
        let id  = $(this).attr('href');
        let top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });

    let upButton = document.createElement('div')
    $(upButton).prependTo('body');
    $(upButton).text('Наверх')
    $(upButton).css({
        'height': '60px',
        'width': '60px',
        'bottom': '30px',
        'right': '30px',
        'cursor': 'pointer',
        'display': 'none',
        'position': 'fixed',
        'color': 'red',
    })

    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 200) {
            if ($(upButton).is(':hidden')) {
                $(upButton).css({opacity : 1}).fadeIn('slow');
            }
        } else { 
            $(upButton).stop(true, false).fadeOut('fast'); 
        }
    
    })
    

    $(upButton).on('click', function() {
        $('html, body').stop().animate({scrollTop : 0}, 1500);
    });

    let slideButton = document.createElement('button');
    $(slideButton).insertAfter('.popular-posts');
    $(slideButton).text('Скрыть').css({
        'margin': ' 0 50%',
    });
    
    $(slideButton).on('click', function slideToggle() {
        $('.popular-posts').toggle('slow');
    })
});