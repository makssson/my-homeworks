import {combineReducers} from "redux";
import checkoutReducer from "./checkoutReducer";
import modalReducer from "./modalReducer";


const reducer = combineReducers({
  modal: modalReducer,
  checkout: checkoutReducer
})


export default reducer;
