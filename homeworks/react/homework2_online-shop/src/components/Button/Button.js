import React, { PureComponent } from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";

class Button extends PureComponent {
  render() {
    const { backCol, text, onClick } = this.props;

    return (
      <>
        <button
          onClick={onClick}
          className={styles.btn}
          style={{ backgroundColor: backCol }}
        >
          {text}
        </button>
      </>
    );
  }
}

Button.propTypes = {
  backCol: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;
