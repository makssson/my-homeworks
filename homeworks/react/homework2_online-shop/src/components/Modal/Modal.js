import React, { PureComponent } from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

class Modal extends PureComponent {
  render() {
    const { header, text, actions, closeButton, close } = this.props;
    console.log(this.props);

    return (
      <>
        <div className={styles.modalWrapper} onClick={close}>
          <div className={styles.modal}>
            <div className={styles.topWrapper}>
              <h1 className={styles.modalTitle}>{header}</h1>
              {closeButton ? (
                <button className={styles.modalClose}>x</button>
              ) : (
                <div></div>
              )}
            </div>

            <p className={styles.modalText}>{text}</p>
            <div className={styles.buttonsWrapper}>{actions}</div>
          </div>
        </div>
      </>
    );
  }
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
  closeButton: PropTypes.bool,
  close: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  closeButton: true,
};

export default Modal;
