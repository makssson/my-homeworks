import React, { PureComponent } from "react";
import Card from "../Card/Card";
import styles from "./Container.module.scss";
import PropTypes from "prop-types";

class Container extends PureComponent {
  render() {
    const { allGoods } = this.props;

    return (
      <section>
        <h1>Goods</h1>
        <div className={styles.goodsContainer}>
          {allGoods.map(({ vc, ...args }) => (
            <Card key={vc} {...args} />
          ))}
        </div>
      </section>
    );
  }
}

Container.propTypes = {
  allGoods: PropTypes.array.isRequired,
};

export default Container;
