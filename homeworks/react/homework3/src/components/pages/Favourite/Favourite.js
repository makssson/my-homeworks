import { React, useState, useEffect } from "react";
import Card from "../../Card/Card";

const Favourite = (props) => {
  const [items, setItems] = useState([]);
  useEffect(() => {
    async function fetchGoods() {
      const result = await fetch("./goods.json").then((res) => res.json());

      setItems(result);
    }
    fetchGoods();
  }, []);

  return (
    <section>
      <h1>Favourite:</h1>
      {items.map(({ vc, name, ...args }) =>
        localStorage.getItem(`${name} Favourite`, "favourite") ? (
          <Card key={vc} name={name} {...args} />
        ) : (
          <Card key={vc} name={name} {...args} display="none"/>
        )
      )}
    </section>
  );
};

export default Favourite;
