import { CHECKOUT_GOODS } from "../actions/checkoutAction";

const initialState = {
  isCheckout: false,
};

const checkoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECKOUT_GOODS: {
      localStorage.clear();
      return { ...state, isCheckout: true };
    }
    default:
      return state;
  }
};

export default checkoutReducer;
