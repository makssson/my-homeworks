import Button from "./Button";
import {render} from "@testing-library/react"


describe('Button render', () => {
    test('Should Button match snapshot', () =>{
        // eslint-disable-next-line testing-library/render-result-naming-convention
        const { asFragment } = render(<Button text="Add to cart" backCol="#b3382c" onClick={() =>{}}/>)

        expect(asFragment()).toMatchSnapshot();
    })
})