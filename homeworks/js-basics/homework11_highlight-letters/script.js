const allButtons = document.querySelectorAll('.btn');

allButtons.forEach(function(elem) {
    document.addEventListener('keyup', function(event) {
        if (event.code === 'Key' + elem.textContent || event.code === elem.textContent) {
            elem.style.backgroundColor = 'blue';
        } else {
            elem.style.backgroundColor = 'black';
        }
    })
})
