import React, { PureComponent } from "react";
import "./App.css";
import Container from "./components/Container/Container";

class App extends PureComponent {
  state = {
    allGoods: [],
  };

  async componentDidMount() {
    const goods = await fetch("./goods.json").then((res) => res.json());
    this.setState({ allGoods: goods });
    console.log(this.state);
  }

  render() {
    const { allGoods } = this.state;

    return (
      <div className="App">
        <Container allGoods={allGoods} />
      </div>
    );
  }
}

export default App;
