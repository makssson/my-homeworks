let myInput = document.createElement('input');
myInput.placeholder = 'Price';
document.body.append(myInput);
myInput.addEventListener('focus', () => {
    myInput.style.outlineColor = 'green';
    myInput.style.display = 'block';
})

myInput.addEventListener('blur', () => {
    if (myInput.value < 0) {
        myInput.style.borderColor = 'red';
        let myMessege = document.createElement('p');
        myMessege.textContent = 'Please enter correct price';
        document.body.append(myMessege)
    } else {
        let mySpan = document.createElement('span');
        document.body.prepend(mySpan);
        mySpan.textContent = 'Текущая цена: ' + `${myInput.value}`;
        let myButton = document.createElement('button');
        myButton.textContent = 'X';
        document.body.prepend(myButton);
        myInput.style.color = 'green';
        myButton.addEventListener('click', () => {
            myButton.remove();
            mySpan.remove();
            myInput.value = 0;
        })
        myInput.style.borderColor = 'grey';
    }
})


